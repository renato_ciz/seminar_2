import json

import streamlit as st
import pyodbc
from pandas import DataFrame
import pandas as pd
from pymongo import MongoClient

sidebar = st.sidebar

class FormPart:
    def __init__(self, id, key, text, name):
        self.id = id
        self.key = key
        self.text = text
        self.name = name
        self.options = {}

    def addOption(self, key, value):
        self.options[key] = value

    def getKey(self):
        return self.key

    def getText(self):
        return self.text

    def getName(self):
        return self.name

    def getOptions(self):
        return self.options

def loadDatabase():
    input = "mongodb+srv://renato:Renato123@cluster0.9olyj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    return MongoClient(input)


if __name__ == '__main__':
    f = open("form.json")
    forms = json.loads(f.read())
    f.close()

    #if 'form' not in st.session_state:
    #    try:

    #            client = MongoClient(st.text_input("Add database stream: "))
                #client = st.session_state['client']

                #db = client.test
                #st.write(db)
                #CONNECTION_STRING = pyodbc.connect('mongodb+srv://filmy:Fer12345678!@cluster0.svmg2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
                #CONNECTION_STRING = pyodbc.connect(str1)
                #client = MongoClient(CONNECTION_STRING)

    #            dbname = client['Seminar_2']
    #            collection_name = dbname['medData']
    #            forms = list(collection_name.find())

    #    except:

    #        f = open("form.json")
    #        forms = json.loads(f.read())
    #        f.close()
    #    st.session_state['form'] = forms

    radioButtons = {}
    for f in forms:
        radioButtons[f['_id']] = f['form']
    formId = sidebar.radio('Select the form you want to fill out: ', list(radioButtons.keys()))

    form = radioButtons.get(formId)
    formConf = []

    for f in form:
        fp = FormPart(f['id'], f['key'], f['text'], f['name'])
        if (f['key'] == 1):
            opt = f['options']
            for o in opt:
                fp.addOption(o['name'], o['id'])
            formConf.append(fp)

        else:
            formConf.append(fp)

    data = {}
    for f in formConf:
        if(f.key == 0):
            st.write(f.text)
        elif(f.key == 1):
            t = st.radio(f.text, f.options.keys())
            data[f.name] = f.options.get(t)
        elif (f.key == 2):
            t = st.text_input(f.text)
            data[f.name] = t
        elif(f.key == 3):
            t = st.number_input(f.text, 0)
            data[f.name] = t
        elif (f.key == 4):
            t = st.date_input(f.text)
            data[f.name] = t
        elif (f.key == 5):
            t = st.checkbox(f.text)
            data[f.name] = int(t)

    st.write(data)