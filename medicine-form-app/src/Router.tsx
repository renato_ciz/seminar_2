import React from 'react';
import './App.css';
import {Divider, Button} from "antd";
import {FormPage as Create} from "./pages/form/FormPage";

export const Router: React.FC = () => {
    const urlParams = new URLSearchParams((window.location.href).split('?')[1]);
    return (
        <div className={'RouterCss'}>
            <Create/>
        </div>
    )
}
