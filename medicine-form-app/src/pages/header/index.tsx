import React from 'react';
import './Header.css';
import {Divider} from "antd";

export const index: React.FC = () => {
    return (
        <Divider className={'Header'}>
            <h1>Medical app</h1>
        </Divider>
    )
}
