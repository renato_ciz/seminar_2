import React, {useState} from 'react';
//import './FormPage.css';
import {List, Input, Radio, Button, InputNumber, DatePicker, Form} from "antd";
import { useFormState } from 'react-final-form';
import axios from 'axios'

interface chkOptions {
    id: number;
    value: number
}

interface selectOptions {
    name: string;
    id: number;
}

interface responseObject {
    key: string;
    value: string;
}

interface inputs {
    id: number;
    key: number;
    text: string;
    options?: selectOptions[];
    response: string;
    name: string;
}

let r = {}


/*let dataSource: inputs[] = [
    {
        id: 0,
        key: 1,
        text: 'Sex',
        response:'',
        name: 'lbl1'
    },
    {
        id: 1,
        key: 2,
        text: 'Input sex of the subject: ',
        options: [{name:'Male', id: 0},{name: 'Female', id: 1}],
        response:'',
        name: 'sex'
    },
    {
        id: 2,
        key: 2,
        text: 'Input education of the subject:',
        options: [
            {name:'Primary education', id: 0},
            {name: 'Lower secondary education', id: 1},
            {name: 'Upper secondary education', id:2},
            {name: 'Post-secondary non-tertiary education', id:3},
            {name: 'Short-cycle tertiary education', id: 4},
            {name:'Bachelor\'s or equivalent', id:5},
            {name:'Master\'s or equivalent', id:6},
            {name:'Doctorate or equivalent', id:7},
        ],
        response:'',
        name: 'education'
    },
    {
        id: 3,
        key: 2,
        text: 'Is subject current smoker: ',
        options: [{name:'True', id: 0},{name: 'False', id: 1}],
        response:'',
        name: 'smoker'
    },
    {
        id: 4,
        key: 4,
        text: 'Number of cigarets per day: ',
        options: [{name:'True', id: 0},{name: 'False', id: 1}],
        response:'',
        name: 'cigsPerDay'
    },

];*/



export const FormPage: React.FC = () => {

    function Test(){
        var t = axios.get(
            'http://localhost:5165/form',
            {headers: {'content-type': 'application/json'}},
        ).then(resp => {console.log(resp.data);
            setDatasource(resp.data);});
        console.log(t)
    }

    const RadioButtons = (prop: inputs) => {
        let ids = prop.options?.map(i => i.id) ?? []
        let names = prop.options?.map(i => i.name) ?? []
        let inp = ''
        const [checked, setChecked] = React.useState<chkOptions[]>();

        const onChange = (e: number, id: number) => {
            let test = checked
            test = test?.filter(i => i.id == id)
            test?.push({id: id, value: e})
            setChecked(test);
        };
        return (
            <div>
                <p>{prop.text}</p>
                {names.map(name => (
                    <Radio.Group name={prop.id.toString()} value={checked}>
                        <Radio value={checked?.filter(i => i.id == prop.id)}
                               onClick={() => {
                                   handleChange(prop.id, names.indexOf(name)?.toString() ?? '', prop.name);
                                   onChange(names.indexOf(name), prop.id)
                               }}>
                            {name}
                        </Radio>
                    </Radio.Group>
                ))}
            </div>
        )
    }

    function Labels(prop: inputs) {
        return (
            <div>
                <p>{prop.text}</p>
            </div>
        )
    }

    function Inputs(prop: inputs) {
        return (
            <div>
                <p>{prop.text}</p>
                <Input onChange={inp => handleChange(prop.id, inp.target.value, prop.name)}/>
            </div>)
    }

    function NumberedInputs(prop: inputs) {
        return (
            <div>
                <p>{prop.text}</p>
                <InputNumber onChange={inp => handleChange(prop.id, inp.toString(), prop.name)}/>
            </div>)
    }

    function DateInput(prop: inputs) {
        return (
            <div>
                <p>{prop.text}</p>
                <DatePicker onChange={inp => handleChange(prop.id, inp?.date().toString() ?? '', prop.name)}/>
            </div>)
    }

    const [dataSource, setDatasource] = React.useState<inputs[]>([])

    const Reload = () => {
        Test();
        /*fetch('http://localhost:5165/form', {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            credentials: "include"
        })
            .then(res => res.json())
            .then(response => {
                console.log(response);
                let res: inputs[] = response
                console.log(res);
                setDatasource(res);
                console.log(dataSource);})
            .catch(err => console.log(err))*/
    }


//const [items, setItems] = useState([]);


    function handleChange(id: number, value: string, keyName: string) {
        let data = dataSource?.find(value1 => value1.id === id) ?? undefined
        if (data === undefined) {
            return;
        }
        setDatasource(dataSource?.filter(value1 =>
            value1.id !== id
        ))
        data.response = value
        dataSource.push(data)
        resp = resp.filter(value1 => value1.key !== keyName)
        resp.push({key: keyName, value: value})
        console.log(resp)

    }

    let resp: responseObject[] = []

    return (
        <div>
            <Button onClick={Reload}>Reload</Button>
            {dataSource !== []?
            <Form>
                <List dataSource={dataSource} renderItem={item =>
                    <Form.Item>
                        {
                            item.key === 1
                                ?
                                <>
                                    {Labels(item)}
                                </>
                                :
                                item.key === 2
                                    ?
                                    <>
                                        {RadioButtons(item)}
                                    </>
                                    :
                                    item.key === 3
                                        ?
                                        <>
                                            {Inputs(item)}
                                        </>
                                        :
                                        item.key === 4
                                            ?
                                            <>
                                                {NumberedInputs(item)}
                                            </>
                                            :
                                            <>
                                                {DateInput(item)}
                                            </>
                        }
                    </Form.Item>
                }
                >


                </List>
            </Form>
                :<></>}
            <Button onClick={() => console.log(dataSource)}>OK</Button>

        </div>

    )
}
