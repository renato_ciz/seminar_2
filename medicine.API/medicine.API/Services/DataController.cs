﻿using System.Text;

namespace medicine.API.Services;

public class DataController
{
    public static string GetFormAppearance()
    {
        using var fileStream =
            new FileStream("testform.json", FileMode.Open, FileAccess.Read);
        using var sr = new StreamReader(fileStream, Encoding.UTF8);
        var data = sr.ReadToEndAsync();
        return data.Result;
    }
}